# Flywheel Dataset API

The Flywheel Dataset API provides a simplified interface for creating, editing and downloading datasets from Flywheel for analytics and machine learning.  With the Dataset API, you can:
- Get a Pandas dataframe from a named data view
- Create custom queries / data views
- Easily download machine learning compatible datasets and folder structures

## Installation

To install the package and all optional dependencies:

```bash
pip install "fw-dataset"
```

Alternatively, add as a `poetry` dependency to your project:

```bash
poetry add fw-dataset
```

## Usage

### Import the Dataset package
```python
from fw_dataset import Dataset
```

### Example: Data Analysis
#### Get a Pandas dataframe for analysis from an existing Flywheel data view
```python
demographics_ds = Dataset(API_KEY, 'fmm/BRaTS2020-Kaggle', 'Subject Demographics')
```
#### Specify a custom query for analysis
```python
# Connect to a project
brats_custom_ds = Dataset(API_KEY, 'fmm/BRaTS2020-Kaggle')

# Define a custom query (data view)
brats_custom_ds.query([
    {'column': 'subject.mlset', 'alias':'ml_set'},
    {'column': 'subject.label', 'alias':'subject'},
    {'column': 'session.info.survival_days', 'alias':'survival_days'}, 
    {'column': 'session.info.age_dec', 'alias':'age'}, 
    {'column': 'session.info.extent_of_resection', 'alias':'extent_of_resection'},
    {'column': 'acquisition.label', 'alias':'acquisition'}, 
    {'column': 'file.name', 'alias':'file_name'}, 
    {'column': 'acquisition.id', 'alias':'acquisition_id'}, 
    {'column': 'file.file_id', 'alias':'file_id'}
  ])

#Render as dataframe
brats_custom_ds.dataframe

```
Rendered dataframe:
![](/images/dataframe01.png)

### Example: Machine Learning

#### Prepare a dataset for ML training
```python
# Connect to a project and a named data view
brats_t1_ds = Dataset(API_KEY, 'fmm/BRaTS2020-Kaggle', 'Training Dataset - T1')

# Use filter() to exclude the test data
brats_t1_ds.filter('mlset != "Testing"')
```
#### Download dataset and related files using a template
```python
#Specify your target project folder, and the target folder structure using template_path
brats_t1_ds.download(destination="projects/project1", template_path="{mlset}/{file_name}", n_jobs=4)
```
#### Review download results
*Note that the exported file path has been appended to the dataset. This provides a complete manifest for audit & tracking purposes but also simplifies local data access.*

```python
brats_t1_ds.dataframe
```
Rendered dataframe:
![](/images/dataframe02.png)

#### Display output structure
```python
brats_t1_ds.print_dir(include_files=True)
```

See [notebooks](notebooks) for more examples.

## Development

Install the project using `poetry` and enable `pre-commit`:

```bash
poetry install
pre-commit install
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
